FROM node:12-alpine
RUN apk add --no-cache python g++ make
WORKDIR /devops-essentials-assignment
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 3000
CMD ["node", "src/index.js"]