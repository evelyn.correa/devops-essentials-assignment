const actions = require('../view/app')

beforeAll(() => {
    document.body.innerHTML = `<div id="root">
        <h3>To do List</h3>
        <div class="addItem">
            <input placeholder="Insert task to add to list" id="inputTask"/>
            <button onclick="addItem(document.getElementById('inputTask'))">Add</button>
        </div>
        <div class="itemsList"></div>
    </div>`;
})

it('Adds item to array', () => {
    const func = actions.addItem({ value: 'testValue' });

    expect(func).toStrictEqual(["testValue"]);
});