let items = [];

function addItem(element) {
    let item = element.value;
    if(item != "") {
        items.push(item);
        element.value = '';
        renderItems();
        return items;
    } else {
        alert("Task cannot be empty!");
    }
}

function renderItems() {
    const list = document.querySelector(".itemsList");

    list.innerHTML = "";

    items.length && items.forEach((i, index) => {
        const newItem = document.createElement('div');
        newItem.className = 'cardItem';
        newItem.id = index;
        newItem.innerHTML = `<p>${i}</p>
        <button onClick="removeItem(${index})">Remove</button>`;
        list.appendChild(newItem);
    });
}

function removeItem(id) {
    items = items.filter(i => items.indexOf(i) !== id);
    renderItems();
}

module.exports = { addItem, renderItems, removeItem, items }